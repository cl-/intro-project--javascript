## Intro 01 - Increment Number #####

Use JavaScript to create a simple page that has a button that can increment a number.

It takes less than 2 minutes if you know JavaScript. If you are new to JavaScript, it may require more time.

* A function that increments the number.

Here are some variations that you can try:

* First, try incrementing the number.
* Next, try decrementing the number.
* Then, try toggling between two numbers. 1 -> 2 -> 1 -> 2 -> 1
* Finally, try iterating across three or more numbers. 1 -> 20 -> 3 -> 40 -> 1 -> 20 -> 3


- - -
## If you don't know Bash #####
* Get the code - Download the [code from the repository as a zip file](https://gitlab.com/cl-/intro-project--javascript/repository/archive.zip) and unzip the zip file
* Open page in web browser - Drag and drop the HTML file into your web browser
* Read [the resources below](#readingResources).


- - -
## If you know Bash #####
### Environment Setup via Bash #####
    #Set your lowercase name
    yourName=INSERT_YOUR_LOWERCASE_NAME_HERE;  #It should look like: yourName=john

    #Set up git repo (only if it has not been set up)
    git clone git@gitlab.com:cl-/intro-project--javascript.git;
    cd intro-project--javascript/intro01-incrementNumber;
    git branch;

    #Set up your branch
    git branch $yourName;
    git checkout $yourName;
    git push -u origin $yourName; #Sets the upstream branch

    #Launch a simple server (optional)
    python -m SimpleHTTPServer;

    #Open page in web browser
    open http://localhost:8000/incrementNumber.html; #after you have already launched the simple server

Read [the resources below](#readingResources).


- - -
### Environment Update via Bash #####
    #Look for your branch name
    git branch;

    #Set your lowercase name
    yourName=INSERT_YOUR_LOWERCASE_NAME_HERE;  #It should look like: yourName=john

    #Get updates
    git checkout $yourName;
    git fetch;
    git rebase master;


- - -
### Resources <a name="readingResources"></a> #####
If you are new to JavaScript, this is sufficient in providing the necessary basic details to get you started.

* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions  
Additional Reading
  * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions
  * https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Statements/function

* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Assignment_Operators
* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators

The resources from [Mozilla](https://developer.mozilla.org/en-US/docs/Web/JavaScript/) are often more reliable than other resources.




