## Intro 02 - Pic Swap #####

Use JavaScript to create a simple page that has a button that can toggle the images.

It takes less than 10 minutes if you know JavaScript. If you are new to JavaScript, it may require more time.

* A function that toggles the images.
* A Swap Pic button.
* When the Swap Pic button is clicked, call the function to toggle the images.

Here are some variations that you can try:

* First, try swapping between two images.
* Next, try iterating across three or more images, shifting by one position with each button click. You can hard code the prcoess, such as by using multiple if-conditions on a fixed array.
* Then, once again, try iterating across three or more images, shifting by one position with each button click. However, this time, it has to be dynamically done by using only one if-condition.


- - -
## If you don't know Bash #####
* Get the code - Download the [code from the repository as a zip file](https://gitlab.com/cl-/intro-project--javascript/repository/archive.zip) and unzip the zip file
* Open page in web browser - Drag and drop the HTML file into your web browser
* Read [the resources below](#readingResources).

## If you know Bash #####
### Environment Setup via Bash #####
    #Set your lowercase name
    yourName=INSERT_YOUR_LOWERCASE_NAME_HERE;  #It should look like: yourName=john

    #Set up git repo (only if it has not been set up)
    git clone git@gitlab.com:cl-/intro-project--javascript.git;
    cd intro-project--javascript/intro02-picSwap;
    git branch;

    #Set up your branch
    git branch $yourName;
    git checkout $yourName;
    git push -u origin $yourName; #Sets the upstream branch

    #Launch a simple server (optional)
    python -m SimpleHTTPServer;

    #Open page in web browser
    open http://localhost:8000/picSwap.html; #after you have already launched the simple server

Read [the resources below](#readingResources).


- - -
### Environment Update via Bash #####
    #Look for your branch name
    git branch;

    #Set your lowercase name
    yourName=INSERT_YOUR_LOWERCASE_NAME_HERE;  #It should look like: yourName=john

    #Get updates
    git checkout $yourName;
    git fetch;
    git rebase master;


- - -
### Resources <a name="readingResources"></a> #####
If you are new to JavaScript, this is sufficient in providing the necessary basic details to get you started.

* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions  
  Additional Reading
  * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions
  * https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Statements/function

Using either addEventListener (in JavaScript) or onclick (in HTML)

* https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener
* https://developer.mozilla.org/en-US/docs/Mozilla/Tech/XUL/Attribute/onclick

#### New Concepts #####
Modifying image

* https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images#Creating_an_image_from_scratch

Adding if-condition

* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/if...else

Data structure

* http://www.javascripter.net/faq/creatingarrays.htm
  * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array

The resources from [Mozilla](https://developer.mozilla.org/en-US/docs/Web/JavaScript/) are often more reliable than other resources.




